import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CatsService } from './cats.service';
import { CreateCatDto } from './dto/create-cat.dto';
import { UpdateCatDto } from './dto/update-cat.dto';
import { Cat } from './schemas/cat.schema';

@Controller('cats')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  @Post()
  create(@Body() createCatDto: CreateCatDto) {
    return this.catsService.create(createCatDto);
  }

  @Get()
  findAll() {
    console.log('findAll');
    return this.catsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.catsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCatDto: UpdateCatDto) {
    return this.catsService.update(+id, updateCatDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.catsService.remove(+id);
  }
}

@Controller('cat/mongoose')
export class CatController {
  constructor(private readonly catsService: CatsService) {}

  @Post()
  async createByMongo(@Body() createCatDto: CreateCatDto) {
    console.log('createByMongo');
    await this.catsService.createByMongo(createCatDto);
  }

  @Get()
  async findAllInMongo(): Promise<Cat[]> {
    console.log('findAllInMongo');
    return this.catsService.findAllInMongo();
  }

  @Get(':id')
  async findOneInMongo(@Param('id') id: string): Promise<Cat> {
    console.log('findOneInMongo');
    return this.catsService.findOneInMongo(id);
  }

  @Delete(':id')
  async deleteInMongo(@Param('id') id: string) {
    return this.catsService.deleteInMongo(id);
  }
}
