import { Module } from '@nestjs/common';
import { FirebaseModule } from '../firebase/firebase.module';
import { ExpenseService } from './expense.service';
import { ExpenseController } from './expense.controller';

@Module({
  imports: [FirebaseModule],
  controllers: [ExpenseController],
  providers: [ExpenseService],
})
export class ExpenseModule {}
