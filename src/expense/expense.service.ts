import { Injectable } from '@nestjs/common';
import { FirebaseRepository } from '../firebase/firebase.repository';
import { CreateExpenseDto } from './dto/create-expense.dto';
import { UpdateExpenseDto } from './dto/update-expense.dto';
import { getFirestore } from 'firebase-admin/firestore';

interface ExpenseDataInterface {
  id: string;
  data: CreateExpenseDto;
}

@Injectable()
export class ExpenseService {
  constructor(private firebaseRepository: FirebaseRepository) {}
  async create(createExpenseDto: CreateExpenseDto) {
    const db = getFirestore();

    await db
      .collection('cities')
      .doc(`${new Date().getTime()}_${Math.abs(Math.random() * 100000)}`)
      .set(createExpenseDto);
    return 'This action adds a new expense';
  }

  async findAll(): Promise<ExpenseDataInterface[] | string> {
    const db = getFirestore();
    const cityRef = db.collection('cities');
    const snapshot = await cityRef.get();
    const snapShotData: ExpenseDataInterface[] = [];
    snapshot.forEach((doc) => {
      const snap = doc.data();
      snapShotData.push({
        id: doc.id,
        data: {
          account: snap.account,
          category: snap.category,
          date: snap.date,
          item: snap.item,
          paymentType: snap.paymentType,
          price: snap.price,
          type: snap.type,
        },
      });
    });
    return snapShotData.length > 0 ? snapShotData : `No such document!`;
  }

  findOne(id: number) {
    return `This action returns a #${id} expense`;
  }

  update(id: number, updateExpenseDto: UpdateExpenseDto) {
    console.log(updateExpenseDto);
    return `This action updates a #${id} expense`;
  }

  async remove(id: string) {
    const db = getFirestore();
    await db.collection('cities').doc(`${id}`).delete();

    return `This action removes a #${id} expense. Status is Done`;
  }
}
